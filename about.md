---
layout: page
title: About
sidebar_link: true
---

<p class="message">
Hi, I'm William.
I'm a backend engineer working @ Vetolib, a booking place for veterinarian and pet owner.
I love Ruby, as it was my very first language and Rails the first framework I've learnt.
Using Ruby drives me to learn Elixir, and thus Phoenix. Functionnal programming helped me write better code.
The promise made by the Crystal team was very appealing, especially for a Rubyist, the fact is that they do not lie.

That's why I focus myself on Ruby, Elixir and Crystal.

I tend to use and learn a lot of other languages such as: Swift, Rust, Elm, Haskell, etc.
</p>

