---
layout: post
title: Functional Algorithm
tags: elixir algorithm leetcode
category: kata
excerpt_separator:  <!--more-->
---
Through the last few years, I've mentored a few folks at Exercism.

Once thing tickles me everytime is how we (as individual) tends to write software.

Often, imperative approach is used to describe what we want to achieve. It's not a bad thing, not at all. You sometimes need to be imperative.

Nevertheless, as a software engineer, I like to use declarative approach. Since we're writing code for people and not machine. I think it's the best I can do to communicate my intent the better way.

<!--more-->

---

## Leetcode

### The problem

Given a 0-indexed integer array nums of size `n`, find the maximum difference between `nums[i]` and `nums[j]` (i.e., `nums[j] - nums[i]`), such that `0 <= i < j < n` and `nums[i] < nums[j]`.

Return the maximum difference. If no such i and j exists, return `-1`.

### Examples

First
```
Input: nums = [7,1,5,4]
Output: 4
Explanation:
The maximum difference occurs with i = 1 and j = 2, nums[j] - nums[i] = 5 - 1 = 4.
Note that with i = 1 and j = 0, the difference nums[j] - nums[i] = 7 - 1 = 6, but i > j, so it is not valid.
```

Second
```
Input: nums = [9,4,3,2]
Output: -1
Explanation:
There is no i and j such that i < j and nums[i] < nums[j].
```

Last
```
Input: nums = [1,5,2,10]
Output: 9
Explanation:
The maximum difference occurs with i = 0 and j = 3, nums[j] - nums[i] = 10 - 1 = 9.
```

### The code

```elixir
def maximum_difference(nums) do
    nums
    |> Enum.scan(&Kernel.min/2)
    |> Enum.zip(nums)
    |> Enum.map(fn {i, j} -> j - i end)
    |> Enum.filter(& &1 != 0)
    |> Enum.max(&>=/2, fn -> -1 end)
end
```

### The logic

The first line is what we call a scan, more precisely, a min scan.

We're scanning the list and applying `&Kernel.min/2` which is a binary function in the sense it takes 2 arguments. An arity of 2 if you prefer.

*Just a minute... What is a scan?*

Refering to the docs, a scan is the following:

```
Applies the given function to each element in the enumerable, 
storing the result in a list and passing it as the accumulator for 
the next computation. 
Uses the first element in the enumerable as the starting value.
```

*And min?*

Well...

```
Returns the smallest of the two given terms according to 
their structural comparison.

If the terms compare equal, the first one is returned.
```

Thus, by scanning our list and applying the min function, it looks like this.


```elixir
list = [7,1,5,4]

Enum.scan(list, fn i, j -> 
    IO.inspect(i, label: "i")
    IO.inspect(j, label: "j")
    Kernel.min(i, j)
end) |> IO.inspect

# i: 1
# j: 7
# i: 5
# j: 1
# i: 4
# j: 1
# [7, 1, 1, 1]
```

We have a "nice" list to work with. Right now it gives us the minimum of the comparison between all the numbers.

The minimum of 7 and 7 is 7.

The minum of the 7 and 1 is 1.

The minum of 1 and 5 is 1.

You got it.

So now, we want the difference between the original input and this new list.
One way to handle this is to make a tuple with the scan value and the old value.

Something like this: `[{7, 7}, {1, 1}, {1, 5}, {1, 4}]`

Eventually, `Enum.zip` or `Stream.zip` backed us.

Once it's done, we just want to get the difference between these two values. So we need to `map` and perform the minus operation.

We ended up with this: `[0, 0, 4, 3]`.

These zeros does not mean any thing for us, since there's no difference at all, so we can filter them.

The rest is pretty straightforward, we filter everything that is different of 0, then we grab the max with `Enum.max`.

## But

There's one missing part, what if we encounter this case `[9,4,3,2]` which expects `-1` as the result?

All the code returns `[]` in this case and it will make `Enum.max` fail.

One thing I've didn't mentionned but a sharp eye already did see, is that `Enum.max` got an arity of 3.

```elixir
max(enumerable, sorter \\ &>=/2, empty_fallback \\ fn -> raise Enum.EmptyError end)
```

So we can pass a sorter and an empty_fallback, that's exactly what we need.

```elixir
nums
# pipeline code
Enum.max(&>=/2, fn -> -1 end)
```

We have to pass the sorter in order to be able to pass the fallback function.

## Wrap up

One quick enhancement is to leverage streams. Since we do not know the length of the passed list, a little bit of optimization is welcomed.

Hopefully, almost every `Enum` functions got a `Stream` equivalent.

Nevertheless, it needs to be evaluated at some point, we need to pass the stream to an `Enum` function at the end.

```elixir
def maximum_difference(nums) do
    nums
    |> Stream.scan(&Kernel.min/2)
    |> Stream.zip(nums)
    |> Stream.map(fn {i, j} -> j - i end)
    |> Stream.filter(& &1 != 0)
    |> Enum.max(&>=/2, fn -> -1 end)
end
```

Until next time!