I"K<p>In my previous post, I’ve talked about Josephus Permutation kata on <a href="http://codewars.com">Codewars</a>.</p>

<p>After taking a short break, it’s time to tackle this exercise.</p>

<p>So what’s the gist of this?</p>

<blockquote>
  <p>This problem takes its name by arguably the most important event in the life of the ancient historian Josephus: according to his tale, he and his 40 soldiers were trapped in a cave by the Romans during a siege.</p>
</blockquote>

<blockquote>
  <p>Refusing to surrender to the enemy, they instead opted for mass suicide, with a twist: they formed a circle and proceeded to kill one man every three, until one last man was left (and that it was supposed to kill himself to end the act).</p>
</blockquote>

<p>Not so funny for a kata though.</p>

<blockquote>
  <p>Well, Josephus and another man were the last two and, as we now know every detail of the story, you may have correctly guessed that they didn’t exactly follow through the original idea.</p>
</blockquote>

<p>Oh great.</p>

<!--more-->
<blockquote>
  <p>You are now to create a function that returns a Josephus permutation, taking as parameters the initial array/list of items to be permuted as if they were in a circle and counted out every k places until none remained.</p>
</blockquote>

<p>So here it starts.
Let’s get the function definition.</p>

<blockquote>
  <p>Tips and notes: it helps to start counting from 1 up to n, instead of the usual range 0..n-1; k will always be &gt;=1.
For example, with n=7 and k=3 <code class="language-plaintext highlighter-rouge">josephus(7,3)</code> should act this way.</p>
</blockquote>

<div class="language-shell highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="o">[</span>1,2,3,4,5,6,7] - initial sequence
<span class="o">[</span>1,2,4,5,6,7] <span class="o">=&gt;</span> 3 is counted out and goes into the result <span class="o">[</span>3]
<span class="o">[</span>1,2,4,5,7] <span class="o">=&gt;</span> 6 is counted out and goes into the result <span class="o">[</span>3,6]
<span class="o">[</span>1,4,5,7] <span class="o">=&gt;</span> 2 is counted out and goes into the result <span class="o">[</span>3,6,2]
<span class="o">[</span>1,4,5] <span class="o">=&gt;</span> 7 is counted out and goes into the result <span class="o">[</span>3,6,2,7]
<span class="o">[</span>1,4] <span class="o">=&gt;</span> 5 is counted out and goes into the result <span class="o">[</span>3,6,2,7,5]
<span class="o">[</span>4] <span class="o">=&gt;</span> 1 is counted out and goes into the result <span class="o">[</span>3,6,2,7,5,1]
<span class="o">[]</span> <span class="o">=&gt;</span> 4 is counted out and goes into the result <span class="o">[</span>3,6,2,7,5,1,4]
</code></pre></div></div>

<p>And so the final result is:</p>

<div class="language-shell highlighter-rouge"><div class="highlight"><pre class="highlight"><code>josephus<span class="o">([</span>1,2,3,4,5,6,7],3<span class="o">)==[</span>3,6,2,7,5,1,4]
</code></pre></div></div>

<p>Rather than going step by step like I did in the previous one. I’m going to write the full code and then commenting the reasons. Furthermore, we’ll go through a solution which is much more idiomatic than mine… and very elegant.</p>

<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">defmodule</span> <span class="no">Josephus</span> <span class="k">do</span>
  <span class="k">def</span> <span class="n">permutation</span><span class="p">(</span><span class="n">items</span><span class="p">,</span> <span class="n">k</span><span class="p">)</span> <span class="k">do</span>
    <span class="n">doperm</span><span class="p">(</span><span class="n">items</span><span class="p">,</span> <span class="n">k</span> <span class="o">-</span> <span class="mi">1</span><span class="p">,</span> <span class="n">k</span><span class="p">,</span> <span class="p">[])</span>
  <span class="k">end</span>

  <span class="k">defp</span> <span class="n">doperm</span><span class="p">([],</span> <span class="n">_i</span><span class="p">,</span> <span class="n">_k</span><span class="p">,</span> <span class="n">r</span><span class="p">),</span> <span class="k">do</span><span class="p">:</span> <span class="no">Enum</span><span class="o">.</span><span class="n">reverse</span><span class="p">(</span><span class="n">r</span><span class="p">)</span> <span class="c1"># Get the list back reversed</span>

  <span class="k">defp</span> <span class="n">doperm</span><span class="p">(</span><span class="n">items</span><span class="p">,</span> <span class="n">i</span><span class="p">,</span> <span class="n">k</span><span class="p">,</span> <span class="n">r</span><span class="p">)</span> <span class="k">do</span>
    <span class="n">j</span> <span class="o">=</span> <span class="n">rem</span><span class="p">(</span><span class="n">i</span><span class="p">,</span> <span class="n">length</span><span class="p">(</span><span class="n">items</span><span class="p">))</span> <span class="c1"># Get the new index</span>
    <span class="p">{</span><span class="n">p</span><span class="p">,</span> <span class="n">items</span><span class="p">}</span> <span class="o">=</span> <span class="no">List</span><span class="o">.</span><span class="n">pop_at</span><span class="p">(</span><span class="n">items</span><span class="p">,</span> <span class="n">j</span><span class="p">)</span> <span class="c1"># pop the item at the given index</span>
    <span class="c1"># Call the function with the new tail and populate the accumulator list with the popped num</span>
    <span class="n">doperm</span><span class="p">(</span><span class="n">items</span><span class="p">,</span> <span class="n">j</span> <span class="o">+</span> <span class="n">k</span> <span class="o">-</span> <span class="mi">1</span><span class="p">,</span> <span class="n">k</span><span class="p">,</span> <span class="p">[</span><span class="n">p</span> <span class="o">|</span> <span class="n">r</span><span class="p">])</span>
  <span class="k">end</span>
<span class="k">end</span>
</code></pre></div></div>

<p>Here, we’ve 3 functions in the <code class="language-plaintext highlighter-rouge">Josephus</code> module, <code class="language-plaintext highlighter-rouge">permutation</code> and two <code class="language-plaintext highlighter-rouge">doperm</code>.</p>

<p>The <code class="language-plaintext highlighter-rouge">defp</code> is a keyword to declare a private function, like in a Ruby file under <code class="language-plaintext highlighter-rouge">private</code> keyword or in a Java class.</p>

<p>So <code class="language-plaintext highlighter-rouge">permutation</code> is somehow the public access to our module and it makes a call to <code class="language-plaintext highlighter-rouge">doperm</code> function, but which one?</p>

<p>Since <code class="language-plaintext highlighter-rouge">Elixir</code> supports multiclauses function like multiples languages, commonly called <code class="language-plaintext highlighter-rouge">overcharging</code>, we can leverage the pattern matching and, compared to the function argument, infer which one will be called.</p>

<p>So the last <code class="language-plaintext highlighter-rouge">doperm</code> function is getting called right now, because all the parameters are needed, and especially, the first one is not an empty list.
Check this out:</p>

<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">defp</span> <span class="n">doperm</span><span class="p">([],</span> <span class="n">_i</span><span class="p">,</span> <span class="n">_k</span><span class="p">,</span> <span class="n">r</span><span class="p">)</span> <span class="c1"># first arg is empty list, _i _k are ignored and r is needed</span>
<span class="k">defp</span> <span class="n">doperm</span><span class="p">(</span><span class="n">items</span><span class="p">,</span> <span class="n">i</span><span class="p">,</span> <span class="n">k</span><span class="p">,</span> <span class="n">r</span><span class="p">)</span> <span class="c1"># first is called items thus a list, and the rest is used too</span>
</code></pre></div></div>

<p>Well, <code class="language-plaintext highlighter-rouge">Elixir</code> is a functional language, so we need to find a way to get a new list with our items popped out of the primary list, <strong><em>mutate</em></strong> it and get the index/key of where we are in this one.</p>

<p>That’s a lot of work but recursion is here. So this is going to be smooth.</p>

<p>In fact, that’s the responsibility of <code class="language-plaintext highlighter-rouge">doperm</code> function, until it gets an empty list.
How this works?</p>

<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">defp</span> <span class="n">doperm</span><span class="p">(</span><span class="n">items</span><span class="p">,</span> <span class="n">i</span><span class="p">,</span> <span class="n">k</span><span class="p">,</span> <span class="n">r</span><span class="p">)</span> <span class="k">do</span>
  <span class="n">j</span> <span class="o">=</span> <span class="n">rem</span><span class="p">(</span><span class="n">i</span><span class="p">,</span> <span class="n">length</span><span class="p">(</span><span class="n">items</span><span class="p">))</span> <span class="c1"># Get the new index</span>
  <span class="p">{</span><span class="n">p</span><span class="p">,</span> <span class="n">items</span><span class="p">}</span> <span class="o">=</span> <span class="no">List</span><span class="o">.</span><span class="n">pop_at</span><span class="p">(</span><span class="n">items</span><span class="p">,</span> <span class="n">j</span><span class="p">)</span> <span class="c1"># pop the item at the given index</span>
  <span class="c1"># Call the function with the new tail and populate the accumulator list with the popped num</span>
  <span class="n">doperm</span><span class="p">(</span><span class="n">items</span><span class="p">,</span> <span class="n">j</span> <span class="o">+</span> <span class="n">k</span> <span class="o">-</span> <span class="mi">1</span><span class="p">,</span> <span class="n">k</span><span class="p">,</span> <span class="p">[</span><span class="n">p</span> <span class="o">|</span> <span class="n">r</span><span class="p">])</span>
<span class="k">end</span>
</code></pre></div></div>

<p>This function waits for 4 arguments, a list, an index, a key and the rest.</p>

<p>NB : excuse my poor naming for variables but … I’ve got no excuse 🤷‍♂️</p>

<p>So I declare a variable <code class="language-plaintext highlighter-rouge">j</code> which is equal to the remainder of the passed index divide by the length of the passed list. This will be where I’ll pop the item from the list.</p>

<p>The <code class="language-plaintext highlighter-rouge">List.pop_at()</code> function returns a tuple if we refer to the doc, it looks like this:</p>

<div class="language-shell highlighter-rouge"><div class="highlight"><pre class="highlight"><code>iex&gt; List.pop_at<span class="o">([</span>1, 2, 3], 0<span class="o">)</span>
<span class="o">{</span>1, <span class="o">[</span>2, 3]<span class="o">}</span>
iex&gt; List.pop_at<span class="o">([</span>1, 2, 3], 5<span class="o">)</span>
<span class="o">{</span>nil, <span class="o">[</span>1, 2, 3]<span class="o">}</span>
</code></pre></div></div>

<p>So I can grab the poped out value, and the updated list. Great.</p>

<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="p">{</span><span class="n">p</span><span class="p">,</span> <span class="n">items</span><span class="p">}</span> <span class="o">=</span> <span class="no">List</span><span class="o">.</span><span class="n">pop_at</span><span class="p">(</span><span class="n">items</span><span class="p">,</span> <span class="n">j</span><span class="p">)</span>
<span class="c1"># pop the item at the given index</span>
<span class="c1"># returns a new items list modified</span>
</code></pre></div></div>

<p>Unfortunately, it’s done. I mean, literally, the last line is just calling our <code class="language-plaintext highlighter-rouge">doperm</code> function with the arguments correctly placed.</p>

<p>Oh we can check this still !</p>

<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="n">doperm</span><span class="p">(</span><span class="n">items</span><span class="p">,</span> <span class="n">j</span> <span class="o">+</span> <span class="n">k</span> <span class="o">-</span> <span class="mi">1</span><span class="p">,</span> <span class="n">k</span><span class="p">,</span> <span class="p">[</span><span class="n">p</span> <span class="o">|</span> <span class="n">r</span><span class="p">])</span>
</code></pre></div></div>

<p>Here, I pass the freshly created items list, based on the return value of the <code class="language-plaintext highlighter-rouge">List.pop_at</code> function. Then, I add <code class="language-plaintext highlighter-rouge">j</code> which was the index where I popped the value at, to <code class="language-plaintext highlighter-rouge">k</code> which is the key, or maybe should have been call it the step, minus 1 because in real life we start counting from 1. Then I still pass the <code class="language-plaintext highlighter-rouge">k</code> or step, and then I update <code class="language-plaintext highlighter-rouge">r</code> or rest with my <code class="language-plaintext highlighter-rouge">p</code>. With the help of the <code class="language-plaintext highlighter-rouge">cons</code> operator <code class="language-plaintext highlighter-rouge">|</code>.</p>

<p>Et voila.</p>

<p>And to be efficient, we need to know how Elixir works under the hood, especially with lists.</p>

<p>When manipulating a list, is easier to add the value at the beginning and reverse it rather than go through each element and place the item popped to the end. Elixir let us do this easily with this piece of syntax <code class="language-plaintext highlighter-rouge">[p|r]</code>, where <code class="language-plaintext highlighter-rouge">p</code> is the <code class="language-plaintext highlighter-rouge">head</code> and <code class="language-plaintext highlighter-rouge">r</code> is the <code class="language-plaintext highlighter-rouge">tail</code>.</p>

<div class="language-shell highlighter-rouge"><div class="highlight"><pre class="highlight"><code>iex&gt; a <span class="o">=</span> <span class="o">[</span>1,2,3,4,5,6,7]
<span class="o">[</span>1,2,3,4,5,6,7]
iex&gt; b <span class="o">=</span> <span class="o">[</span><span class="nt">-2</span>,-1,0 | a]
<span class="o">[</span><span class="nt">-2</span>,-1,0,1,2,3,4,5,6,7]
</code></pre></div></div>

<p>I’ve already talked of this feature of Elixir <a href="https://www.maartz.tech/posts/2020-04-16-ex-js.html">in a previous post</a>.</p>

<p>Still, we got a list but, reversed… That’s the job of <code class="language-plaintext highlighter-rouge">doperm</code>. Hooray.</p>

<h2 id="what-">What ??</h2>

<p>Yes.</p>

<p>First <code class="language-plaintext highlighter-rouge">doperm</code> function clause was:</p>

<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">defp</span> <span class="n">doperm</span><span class="p">([],</span> <span class="n">_i</span><span class="p">,</span> <span class="n">_k</span><span class="p">,</span> <span class="n">r</span><span class="p">)</span>
</code></pre></div></div>

<p>We wait for an empty list as the first argument. We do not care of the index and the key OR step and we expect the rest. And when we get <code class="language-plaintext highlighter-rouge">r</code> we just pass it through the <code class="language-plaintext highlighter-rouge">Enum.reverse</code> function to … reverse it.</p>

<p>So we’ve constructed our list idiomatically without losing performance and we go through it once to reverse it. So far so good. Our mission is over.</p>

<p>This was a tricky exercise for a newcomer like me in the Elixir world, in the functional programming paradigm, but it was challenging and that’s what we love, don’t we ?</p>

<p>And for the more elegant and idiomatic code, see:</p>

<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">defmodule</span> <span class="no">Josephus</span> <span class="k">do</span>
  <span class="k">def</span> <span class="n">permutation</span><span class="p">(</span><span class="n">items</span><span class="p">,</span> <span class="n">k</span><span class="p">)</span> <span class="k">do</span>
    <span class="no">Stream</span><span class="o">.</span><span class="n">unfold</span><span class="p">({</span><span class="n">items</span><span class="p">,</span> <span class="n">length</span><span class="p">(</span><span class="n">items</span><span class="p">)</span> <span class="o">-</span> <span class="mi">1</span><span class="p">},</span> <span class="k">fn</span>
      <span class="p">{[],</span> <span class="n">_</span><span class="p">}</span> <span class="o">-&gt;</span>
        <span class="no">nil</span>
      
      <span class="p">{</span><span class="n">it</span><span class="p">,</span> <span class="n">index</span><span class="p">}</span> <span class="o">-&gt;</span>
        <span class="n">index</span> <span class="o">=</span> <span class="n">rem</span><span class="p">(</span><span class="n">k</span> <span class="o">+</span> <span class="n">index</span><span class="p">,</span> <span class="n">length</span><span class="p">(</span><span class="n">it</span><span class="p">))</span>
        <span class="p">{</span><span class="n">val</span><span class="p">,</span> <span class="n">it_prime</span><span class="p">}</span> <span class="o">=</span> <span class="no">List</span><span class="o">.</span><span class="n">pop_at</span><span class="p">(</span><span class="n">it</span><span class="p">,</span> <span class="n">index</span><span class="p">)</span>
        
        <span class="p">{</span><span class="n">val</span><span class="p">,</span> <span class="p">{</span><span class="n">it_prime</span><span class="p">,</span> <span class="n">index</span> <span class="o">-</span> <span class="mi">1</span><span class="p">}}</span>
        
    <span class="k">end</span><span class="p">)</span>
    <span class="o">|&gt;</span> <span class="no">Enum</span><span class="o">.</span><span class="n">to_list</span><span class="p">()</span>
  <span class="k">end</span>
<span class="k">end</span>
</code></pre></div></div>

<p>This is a beautiful piece of Elixir code.
Which for performance sake use the lazy version of <code class="language-plaintext highlighter-rouge">Enum.unfold</code>, <code class="language-plaintext highlighter-rouge">Stream.unfold</code>.
Streams are very powerful to manipulate undetermined potentially gigantic computation.</p>

<p>They probably worth a post.</p>
:ET