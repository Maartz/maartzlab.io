I"�0<p>For this kata, I’d like to focus on readability.
The kata is “Valid Parentheses”, here’s the gist.</p>

<blockquote>
  <p>Write a function that takes a string of parentheses, and determines if the order of the parentheses is valid. The function should return true if the string is valid, and false if it’s invalid.</p>
</blockquote>

<p>And we got some examples:</p>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>"()"              =&gt;  true
")(()))"          =&gt;  false
"("               =&gt;  false
"(())((()())())"  =&gt;  true
</code></pre></div></div>

<p>So in the stub provided by Codewars, we got this.</p>

<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">defmodule</span> <span class="no">ParenthesesValidator</span> <span class="k">do</span>
  <span class="k">def</span> <span class="n">valid_parentheses</span><span class="p">(</span><span class="n">string</span><span class="p">)</span> <span class="k">do</span>
     <span class="c1"># it's empty here   </span>
  <span class="k">end</span>
<span class="k">end</span>
</code></pre></div></div>

<!--more-->

<p>First thing first, we need to get the value from the <code class="language-plaintext highlighter-rouge">string</code> parameter and transform it in a list.</p>

<p>One could do:</p>
<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="no">String</span><span class="o">.</span><span class="n">split</span><span class="p">(</span><span class="s2">"()"</span><span class="p">,</span> <span class="s2">""</span><span class="p">)</span>
<span class="o">=&gt;</span> <span class="p">[</span><span class="s2">""</span><span class="p">,</span> <span class="s2">"("</span><span class="p">,</span> <span class="s2">")"</span><span class="p">,</span> <span class="s2">""</span><span class="p">]</span>
</code></pre></div></div>
<p>But the returned list is dirty with the opening and closing <code class="language-plaintext highlighter-rouge">""</code> empty string.</p>

<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="no">String</span><span class="o">.</span><span class="n">split</span><span class="p">(</span><span class="s2">"()"</span><span class="p">,</span> <span class="s2">""</span><span class="p">,</span> <span class="ss">trim:</span> <span class="no">true</span><span class="p">)</span>
<span class="o">=&gt;</span> <span class="p">[</span><span class="s2">"("</span><span class="p">,</span> <span class="s2">")"</span><span class="p">]</span>
</code></pre></div></div>
<p>So we could trim it.</p>

<p>But we also could leverage <code class="language-plaintext highlighter-rouge">String.graphemes/1</code></p>

<blockquote>
  <p>@spec graphemes(t()) :: [grapheme()]
delegate_to: String.Unicode.graphemes/1
Returns Unicode graphemes in the string as per Extended Grapheme Cluster
algorithm.
The algorithm is outlined in the Unicode Standard Annex #29, Unicode Text
Segmentation (https://www.unicode.org/reports/tr29/).
For details about code points and graphemes, see the String module
documentation.</p>
  <h2 id="examples">Examples</h2>
  <div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>iex&gt; String.graphemes("Ńaïve")
["Ń", "a", "ï", "v", "e"]
iex&gt; String.graphemes("\u00e9")
["é"]
iex&gt; String.graphemes("\u0065\u0301")
["é"]
</code></pre></div>  </div>
</blockquote>

<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="no">String</span><span class="o">.</span><span class="n">graphemes</span><span class="p">(</span><span class="s2">"(())()())"</span><span class="p">)</span>
<span class="o">=&gt;</span> <span class="p">[</span><span class="s2">"("</span><span class="p">,</span> <span class="s2">"("</span><span class="p">,</span> <span class="s2">")"</span><span class="p">,</span> <span class="s2">")"</span><span class="p">,</span> <span class="s2">"("</span><span class="p">,</span> <span class="s2">")"</span><span class="p">,</span> <span class="s2">"("</span><span class="p">,</span> <span class="s2">")"</span><span class="p">,</span> <span class="s2">")"</span><span class="p">]</span>
</code></pre></div></div>
<p>Good, no need to trim and no extra empty strings.</p>

<p>A famous cooker once said, “The key for a great dish is a good reduction, it also work in life in general”.
He probably knows <code class="language-plaintext highlighter-rouge">Enum.reduce</code> from Elixir library.
And we’re going to use it.</p>

<p>In fact, it’s better to know what we want to achieve.
The main goal is to be able to determine if we’ve a closing parentheses for an opened one.</p>

<p>So declare an accumulator which holds the count of encountered “(“ and “)”.
Furthermore, we’d like to increment the count of this acc when it’s a “(“ and decrement when it’s a “)”.</p>

<p>And compare the accumulator to 0 because that’d say we’ve for each “(“ a “)”.</p>

<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">defmodule</span> <span class="no">ParenthesesValidator</span> <span class="k">do</span>
  <span class="k">def</span> <span class="n">valid_parentheses</span><span class="p">(</span><span class="n">string</span><span class="p">)</span> <span class="k">do</span>
        <span class="n">string</span> 
        <span class="o">|&gt;</span> <span class="no">String</span><span class="o">.</span><span class="n">graphemes</span>
        <span class="o">|&gt;</span> <span class="no">Enum</span><span class="o">.</span><span class="n">reduce</span><span class="p">(</span><span class="mi">0</span><span class="p">,</span> <span class="k">fn</span> <span class="n">x</span><span class="p">,</span> <span class="n">acc</span> <span class="o">-&gt;</span> 
            <span class="k">case</span> <span class="n">x</span> <span class="k">do</span>
                <span class="s2">"("</span> <span class="o">-&gt;</span> <span class="n">acc</span> <span class="o">+</span> <span class="mi">1</span>
                <span class="s2">")"</span> <span class="o">-&gt;</span> <span class="n">acc</span> <span class="o">-</span> <span class="mi">1</span> 
                <span class="n">_</span> <span class="o">-&gt;</span> <span class="n">acc</span>
            <span class="k">end</span>
        <span class="k">end</span><span class="p">)</span> <span class="o">==</span> <span class="mi">0</span>
  <span class="k">end</span>
<span class="k">end</span>
</code></pre></div></div>

<p>That’s the what we get by using the reduce function.
Inside the modifying function, we use a <code class="language-plaintext highlighter-rouge">case</code> clause to increment or decrement the accumulator value.
At the end of the reduction, we compare it to 0 using <code class="language-plaintext highlighter-rouge">== 0</code> to return <code class="language-plaintext highlighter-rouge">true</code> or <code class="language-plaintext highlighter-rouge">false</code>.</p>

<p>So… it’s done right ?</p>

<p>Not really. I omit this test in the suit.</p>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>")("              =&gt;  false
</code></pre></div></div>

<p>But we got a closing and opening parentheses and our accumulator is equal to 0.
So in fact it’s not only related to the count but also to the order of apparition.
Thus, we should not be able increment/decrement if the counter is bellow zero. Which means that a closing parentheses would put the accumulator in <code class="language-plaintext highlighter-rouge">-1</code> state.
Then, we should not be able to update it. And return false.</p>

<p>Hopefully, guard clause are here for the rescue.
Elixir’s guard clauses are directly inherited from Erlang’s one.
One must know that you cannot execute function call in guard clause.</p>

<p>Imagine you’d like to guard on the length of a string, like this:</p>

<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="ow">when</span> <span class="no">String</span><span class="o">.</span><span class="n">length</span><span class="p">(</span><span class="n">string</span><span class="p">)</span> <span class="o">&gt;</span> <span class="mi">10</span> <span class="c1"># would fail</span>
</code></pre></div></div>

<p>But you can do this:</p>
<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="ow">when</span> <span class="n">byte_size</span><span class="p">(</span><span class="n">string</span><span class="p">)</span> <span class="o">&gt;</span> <span class="mi">10</span> <span class="c1"># 🎉</span>
</code></pre></div></div>

<p>A little [guard clause cheatsheet].(https://kapeli.com/cheat_sheets/Elixir_Guards.docset/Contents/Resources/Documents/index)</p>

<p>So it’s good to know that we got powerful tools to describe what we want to achieve.</p>

<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">defmodule</span> <span class="no">ParenthesesValidator</span> <span class="k">do</span>
  <span class="k">def</span> <span class="n">valid_parentheses</span><span class="p">(</span><span class="n">string</span><span class="p">)</span> <span class="k">do</span>
        <span class="n">string</span> 
        <span class="o">|&gt;</span> <span class="no">String</span><span class="o">.</span><span class="n">graphemes</span>
        <span class="o">|&gt;</span> <span class="no">Enum</span><span class="o">.</span><span class="n">reduce</span><span class="p">(</span><span class="mi">0</span><span class="p">,</span> <span class="k">fn</span> <span class="n">x</span><span class="p">,</span> <span class="n">acc</span> <span class="o">-&gt;</span> 
            <span class="k">case</span> <span class="n">x</span> <span class="k">do</span>
                <span class="s2">"("</span> <span class="ow">when</span> <span class="n">acc</span> <span class="o">&gt;=</span> <span class="mi">0</span> <span class="o">-&gt;</span> <span class="n">acc</span> <span class="o">+</span> <span class="mi">1</span>
                <span class="s2">")"</span> <span class="ow">when</span> <span class="n">acc</span> <span class="o">&gt;=</span> <span class="mi">0</span> <span class="o">-&gt;</span> <span class="n">acc</span> <span class="o">-</span> <span class="mi">1</span> 
                <span class="n">_</span> <span class="o">-&gt;</span> <span class="n">acc</span>
            <span class="k">end</span>
        <span class="k">end</span><span class="p">)</span> <span class="o">==</span> <span class="mi">0</span>
  <span class="k">end</span>
<span class="k">end</span>
</code></pre></div></div>

<p>Here’s the final version of it.
Reduce, case with guard are the bread and butter of Elixir, being able to use them will help you achieve more easily your goals.</p>

<p>Thanks for reading and happy coding.</p>

<p>Once again, a nice one:</p>

<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">defmodule</span> <span class="no">ParenthesesValidator</span> <span class="k">do</span>
  <span class="k">def</span> <span class="n">valid_parentheses</span><span class="p">(</span><span class="n">string</span><span class="p">)</span> <span class="k">do</span>
    <span class="k">case</span> <span class="no">Regex</span><span class="o">.</span><span class="n">compile</span><span class="p">(</span><span class="n">string</span><span class="p">)</span> <span class="k">do</span>
      <span class="p">{</span><span class="ss">:ok</span><span class="p">,</span> <span class="n">_</span><span class="p">}</span> <span class="o">-&gt;</span> <span class="no">true</span>
      <span class="p">{</span><span class="ss">:error</span><span class="p">,</span> <span class="n">_</span><span class="p">}</span> <span class="o">-&gt;</span> <span class="no">false</span>
    <span class="k">end</span>
  <span class="k">end</span>
<span class="k">end</span>
</code></pre></div></div>
<p>Hey but it’s a Regex, that’s cheating 😂😂😂</p>
:ET