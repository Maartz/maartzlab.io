I"L<p>So I was doing some JS today, despite everything we can say about Javascript. It has a great tooling environment. It helps a lot to have standards. Funky coding style or inconsistency can lead to misunderstanding of some concepts.</p>

<p>It’s not a secret, I’m learning Elixir. And I’m doing this differently than all the others.</p>

<p><img src="https://media.giphy.com/media/q1mHcB8wOCWf6/giphy.gif" alt="me" /></p>

<h2 id="by-reading-books">By reading books.</h2>

<p>That’s quite new for me. Before, I used to go on Youtube, Codecademy or Udemy. Watching other people code what I wanted to code. It was a passive way of learning. No problem-solving. It’s a soft and warm place.</p>

<!--more-->

<p>Anyway, I was wondering if Elixir has one of these tools. And the fact is that it has. It’s called <a href="https://github.com/rrrene/credo">Credo</a>.</p>

<h2 id="credo">Credo</h2>

<p>With more than 130 contributors, 2K commits and 3.3k stars on Github it seems that it is the defacto tool for static code analysis in Elixir. I’m so grateful that some people out there built a tool like this one.
Enforcing code style and give some hints on how it could be better is a nice thing to have.</p>

<p>Furthermore, Credo comes with a lot of configuration capabilities.</p>

<h3 id="in-action">In action</h3>

<p>Simply add credo to your <code class="language-plaintext highlighter-rouge">mix.exs</code> file to get credo.</p>
<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">defp</span> <span class="n">deps</span> <span class="k">do</span>
  <span class="p">[</span>
    <span class="p">{</span><span class="ss">:credo</span><span class="p">,</span> <span class="s2">"~&gt; 1.2"</span><span class="p">,</span> <span class="ss">only:</span> <span class="p">[</span><span class="ss">:dev</span><span class="p">,</span> <span class="ss">:test</span><span class="p">],</span> <span class="ss">runtime:</span> <span class="no">false</span><span class="p">}</span>
  <span class="p">]</span>
<span class="k">end</span>
</code></pre></div></div>

<p>And that’s pretty much it, it runs out of the box.
To get an analysis, type:</p>
<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>mix credo
</code></pre></div></div>

<p>And to enforce some strict styling guide</p>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>mix credo --strict
</code></pre></div></div>

<p>This is the kind of output you can wait to have by using credo.</p>

<p><img src="/assets/img/credo2.png" alt="Credo's output in terminal" /></p>

<p>The arrow ‘<code class="language-plaintext highlighter-rouge">-&gt;</code>’ shows up the priority of the issue.
So far so good, we have now a very good tool to help us styling out our code regarding standard guide.</p>

<p>I’ve didn’t feel the use of Credo, even in little project, but I’m changing my mind. You often go for Prettier and/or JS/TSLint while doing JS or Rubocop while doing Ruby. Why Elixir wouldn’t be the same ?</p>
:ET