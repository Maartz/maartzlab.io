I"�(<p>Through the last few years, I’ve mentored a few folks at Exercism.</p>

<p>Once thing tickles me everytime is how we (as individual) tends to write software.</p>

<p>Often, imperative approach is used to describe what we want to achieve. It’s not a bad thing, not at all. You sometimes need to be imperative.</p>

<p>Nevertheless, as a software engineer, I like to use declarative approach. Since we’re writing code for people and not machine. I think it’s the best I can do to communicate my intent the better way.</p>

<!--more-->

<hr />

<h2 id="leetcode">Leetcode</h2>

<h3 id="the-problem">The problem</h3>

<p>Given a 0-indexed integer array nums of size <code class="language-plaintext highlighter-rouge">n</code>, find the maximum difference between <code class="language-plaintext highlighter-rouge">nums[i]</code> and <code class="language-plaintext highlighter-rouge">nums[j]</code> (i.e., <code class="language-plaintext highlighter-rouge">nums[j] - nums[i]</code>), such that <code class="language-plaintext highlighter-rouge">0 &lt;= i &lt; j &lt; n</code> and <code class="language-plaintext highlighter-rouge">nums[i] &lt; nums[j]</code>.</p>

<p>Return the maximum difference. If no such i and j exists, return <code class="language-plaintext highlighter-rouge">-1</code>.</p>

<h3 id="examples">Examples</h3>

<p>First</p>
<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>Input: nums = [7,1,5,4]
Output: 4
Explanation:
The maximum difference occurs with i = 1 and j = 2, nums[j] - nums[i] = 5 - 1 = 4.
Note that with i = 1 and j = 0, the difference nums[j] - nums[i] = 7 - 1 = 6, but i &gt; j, so it is not valid.
</code></pre></div></div>

<p>Second</p>
<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>Input: nums = [9,4,3,2]
Output: -1
Explanation:
There is no i and j such that i &lt; j and nums[i] &lt; nums[j].
</code></pre></div></div>

<p>Last</p>
<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>Input: nums = [1,5,2,10]
Output: 9
Explanation:
The maximum difference occurs with i = 0 and j = 3, nums[j] - nums[i] = 10 - 1 = 9.
</code></pre></div></div>

<h3 id="the-code">The code</h3>

<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">def</span> <span class="n">maximum_difference</span><span class="p">(</span><span class="n">nums</span><span class="p">)</span> <span class="k">do</span>
    <span class="n">nums</span>
    <span class="o">|&gt;</span> <span class="no">Stream</span><span class="o">.</span><span class="n">scan</span><span class="p">(</span><span class="o">&amp;</span><span class="no">Kernel</span><span class="o">.</span><span class="n">min</span><span class="o">/</span><span class="mi">2</span><span class="p">)</span>
    <span class="o">|&gt;</span> <span class="no">Stream</span><span class="o">.</span><span class="n">zip</span><span class="p">(</span><span class="n">nums</span><span class="p">)</span>
    <span class="o">|&gt;</span> <span class="no">Stream</span><span class="o">.</span><span class="n">map</span><span class="p">(</span><span class="k">fn</span> <span class="p">{</span><span class="n">i</span><span class="p">,</span> <span class="n">j</span><span class="p">}</span> <span class="o">-&gt;</span> <span class="n">j</span> <span class="o">-</span> <span class="n">i</span> <span class="k">end</span><span class="p">)</span>
    <span class="o">|&gt;</span> <span class="no">Stream</span><span class="o">.</span><span class="n">filter</span><span class="p">(</span><span class="o">&amp;</span> <span class="nv">&amp;1</span> <span class="o">!=</span> <span class="mi">0</span><span class="p">)</span>
    <span class="o">|&gt;</span> <span class="no">Enum</span><span class="o">.</span><span class="n">max</span><span class="p">(</span><span class="o">&amp;&gt;=/</span><span class="mi">2</span><span class="p">,</span> <span class="k">fn</span> <span class="o">-&gt;</span> <span class="o">-</span><span class="mi">1</span> <span class="k">end</span><span class="p">)</span>
<span class="k">end</span>
</code></pre></div></div>

<h3 id="the-logic">The logic</h3>

<p>The first line is what we call a scan, more precisely, a min scan.</p>

<p>We’re scanning the list and applying <code class="language-plaintext highlighter-rouge">&amp;Kernel.min/2</code> which is a binary function in the sense it takes 2 arguments. An arity of 2 if you prefer.</p>

<p><em>Just a minute… What is a scan?</em></p>

<p>Refering to the docs, a scan is the following:</p>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>Applies the given function to each element in the enumerable, 
storing the result in a list and passing it as the accumulator for 
the next computation. 
Uses the first element in the enumerable as the starting value.
</code></pre></div></div>

<p><em>And min?</em></p>

<p>Well…</p>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>Returns the smallest of the two given terms according to 
their structural comparison.

If the terms compare equal, the first one is returned.
</code></pre></div></div>

<p>Thus, by scanning our list and applying the min function, it looks like this.</p>

<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="n">list</span> <span class="o">=</span> <span class="p">[</span><span class="mi">7</span><span class="p">,</span><span class="mi">1</span><span class="p">,</span><span class="mi">5</span><span class="p">,</span><span class="mi">4</span><span class="p">]</span>

<span class="no">Enum</span><span class="o">.</span><span class="n">scan</span><span class="p">(</span><span class="n">list</span><span class="p">,</span> <span class="k">fn</span> <span class="n">i</span><span class="p">,</span> <span class="n">j</span> <span class="o">-&gt;</span> 
    <span class="no">IO</span><span class="o">.</span><span class="n">inspect</span><span class="p">(</span><span class="n">i</span><span class="p">,</span> <span class="ss">label:</span> <span class="s2">"i"</span><span class="p">)</span>
    <span class="no">IO</span><span class="o">.</span><span class="n">inspect</span><span class="p">(</span><span class="n">j</span><span class="p">,</span> <span class="ss">label:</span> <span class="s2">"j"</span><span class="p">)</span>
    <span class="no">Kernel</span><span class="o">.</span><span class="n">min</span><span class="p">(</span><span class="n">i</span><span class="p">,</span> <span class="n">j</span><span class="p">)</span>
<span class="k">end</span><span class="p">)</span> <span class="o">|&gt;</span> <span class="no">IO</span><span class="o">.</span><span class="n">inspect</span>

<span class="c1"># i: 1</span>
<span class="c1"># j: 7</span>
<span class="c1"># i: 5</span>
<span class="c1"># j: 1</span>
<span class="c1"># i: 4</span>
<span class="c1"># j: 1</span>
<span class="c1"># [7, 1, 1, 1]</span>
</code></pre></div></div>

<p>We have a “nice” list to work with. Right now it gives us the minimum of the comparison between all the numbers.</p>

<p>The minimum of 7 and 7 is 7.</p>

<p>The minum of the 7 and 1 is 1.</p>

<p>The minum of 1 and 5 is 1.</p>

<p>You got it.</p>

<p>So now, we want the difference between the original input and this new list.
One way to handle this is to make a tuple with the scan value and the old value.</p>

<p>Something like this: <code class="language-plaintext highlighter-rouge">[{7, 7}, {1, 1}, {1, 5}, {1, 4}]</code></p>

<p>Eventually, <code class="language-plaintext highlighter-rouge">Enum.zip</code> or <code class="language-plaintext highlighter-rouge">Stream.zip</code> backed us.</p>

<p>Once it’s done, we just want to get the difference between these two values. So we need to <code class="language-plaintext highlighter-rouge">map</code> and perform the minus operation.</p>

<p>We ended up with this: <code class="language-plaintext highlighter-rouge">[0, 0, 4, 3]</code>.</p>

<p>These zeros does not mean any thing for us, since there’s no difference at all, so we can filter them.</p>

<p>The rest is pretty straightforward, we filter everything that is different of 0, then we grab the max with <code class="language-plaintext highlighter-rouge">Enum.max</code>.</p>

<h2 id="but">But</h2>

<p>There’s one missing part, what if we encounter this case <code class="language-plaintext highlighter-rouge">[9,4,3,2]</code> which expects <code class="language-plaintext highlighter-rouge">-1</code> as the result?</p>

<p>All the code returns <code class="language-plaintext highlighter-rouge">[]</code> in this case and it will make <code class="language-plaintext highlighter-rouge">Enum.max</code> fail.</p>

<p>One thing I’ve didn’t mentionned but a sharp eye already did see, is that <code class="language-plaintext highlighter-rouge">Enum.max</code> got an arity of 3.</p>

<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="n">max</span><span class="p">(</span><span class="n">enumerable</span><span class="p">,</span> <span class="n">sorter</span> <span class="p">\\</span> <span class="o">&amp;&gt;=/</span><span class="mi">2</span><span class="p">,</span> <span class="n">empty_fallback</span> <span class="p">\\</span> <span class="k">fn</span> <span class="o">-&gt;</span> <span class="k">raise</span> <span class="no">Enum</span><span class="o">.</span><span class="no">EmptyError</span> <span class="k">end</span><span class="p">)</span>
</code></pre></div></div>

<p>So we can pass a sorter and an empty_fallback, that’s exactly what we need.</p>

<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="n">nums</span>
<span class="c1"># pipeline code</span>
<span class="no">Enum</span><span class="o">.</span><span class="n">max</span><span class="p">(</span><span class="o">&amp;&gt;=/</span><span class="mi">2</span><span class="p">,</span> <span class="k">fn</span> <span class="o">-&gt;</span> <span class="o">-</span><span class="mi">1</span> <span class="k">end</span><span class="p">)</span>
</code></pre></div></div>

<p>We have to pass the sorter in order to be able to pass the fallback function.</p>
:ET